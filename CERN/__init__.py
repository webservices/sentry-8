"""
CERN authentication using REMOTE_USER

This module implements authentication for Sentry 8.22 based on `REMOTE_USER`.
Sentry 8.22 uses django 1.6.11, we therefore use the respective documentation:
<https://django.readthedocs.io/en/1.6.x/howto/auth-remote-user.html>.
"""

# Since we rewrite the `process_request` method from `RemoteUserMiddleware`
# we need to import `auth` from `django.contrib` for the `auth.authenticate`
# and `auth.login` calls as well as the `ImproperlyConfigured` exception from
# `django.core.exceptions`.
from django.contrib import auth
from django.core.exceptions import ImproperlyConfigured

# We import the global `settings` to get `CERN_ADMINS` (to know when to assign
# the superuser status upon first login) and `SENTRY_SINGLE_ORGANIZATION` that
# should always be `True` in our set up.
from django.conf import settings

# We import the 2 classes (middleware and backend) that we inherit from to
# customize the REMOTE_USER authentication.
from django.contrib.auth.backends import RemoteUserBackend
from django.contrib.auth.middleware import RemoteUserMiddleware

# In case this is a first login we need to assign this user to the default
# organization (note the need for `SENTRY_SINGLE_ORGANIZATION` being `True`)
# with the proper role and possibly add them to one or more teams. We therefore
# import the respective modules (`roles` and several models).
from sentry import roles
from sentry.models import (
    Organization,
    OrganizationMember,
    OrganizationMemberTeam,
    Team,
    User,
    UserEmail
)


class CERNRemoteUserMiddleware(RemoteUserMiddleware):
    """
    Our custom middleware inherits from the default `RemoteUserMiddleware`
    django middleware: <django/django/contrib/auth/middleware.py> in the
    `stable/1.6.x` branch.
    """

    # We define the custom HTTP request headers we send through from the web
    # server. There are 3 headers, 1 for the username, 1 for the e-mail and
    # 1 for the fullname. Note that django processes HTTP request headers and
    # prepends `HTTP_` as well as replaces `-` with `_`. Thefefore for example
    # `X-Auth_User` becomes `HTTP_X_AUTH_USER`.
    username_header = "HTTP_X_ADFS_LOGIN"
    email_header = "HTTP_X_ADFS_EMAIL"
    name_header = "HTTP_X_ADFS_FULLNAME"

    def process_request(self, request):
        """
        We overwrite the default `process_request` method from
        `RemoteUserMiddleware` to handle our custom HTTP request headers.

        The original code and comments are intact and we only comment on our
        custom changes.
        """

        # AuthenticationMiddleware is required so that request.user exists.
        if not hasattr(request, 'user'):
            raise ImproperlyConfigured(
                "The Django remote user auth middleware requires the"
                " authentication middleware to be installed.  Edit your"
                " MIDDLEWARE_CLASSES setting to insert"
                " 'django.contrib.auth.middleware.AuthenticationMiddleware'"
                " before the RemoteUserMiddleware class.")
        try:
            # We get the `username`, `email` and `name` from the respective
            # custom headers.
            username = request.META[self.username_header]
            email = request.META[self.email_header]
            name = request.META[self.name_header]
        except KeyError:
            # If specified header doesn't exist then remove any existing
            # authenticated remote-user, or return (leaving request.user set to
            # AnonymousUser by the AuthenticationMiddleware).
            if request.user.is_authenticated():
                self._remove_invalid_user(request)
            return
        # If the user is already authenticated and that user is the user we are
        # getting passed in the headers, then the correct user is already
        # persisted in the session and we don't need to continue.
        if request.user.is_authenticated():
            if request.user.get_username() == self.clean_username(
                username,
                request
            ):
                return
            else:
                # An authenticated user is associated with the request, but
                # it does not match the authorized user in the header.
                self._remove_invalid_user(request)

        # We are seeing this user for the first time in this session, attempt
        # to authenticate the user.
        # We call `auth.authenticate` with our custom arguments including the
        # `username`, `email` and `name`.
        user = auth.authenticate(
            username=username,
            email=email,
            name=name
        )
        if user:
            # User is valid.  Set request.user and persist user in the session
            # by logging the user in.
            request.user = user
            auth.login(request, user)


class CERNRemoteUserBackend(RemoteUserBackend):
    """
    Our custom backend inherits from the default `RemoteUserBackend`
    django backend: <django/django/contrib/auth/backends.py> in the
    `stable/1.6.x` branch.
    """

    # By default make sure a new user is created upon first login.
    create_unknown_user = True

    def authenticate(self, username, email, name):
        """
        We overwrite the default `authenticate` method from
        `RemoteUserBackend` to handle our custom arguments (`username`, `email`
        and `name` instead of `remote_user`).

        The original code and comments are intact and we only comment on our
        custom changes.
        """

        # `remote_user` is `username` for us.
        if not username:
            return
        user = None
        username = self.clean_username(username)

        # We don't need to get the `UserModel`, we just import the actual
        # `User` model from `sentry`.

        # We prepare a dictionary with the user attributes(`username`, `email`
        # and `name`) that we then pass to the user creation.
        user_attributes = {
            User.USERNAME_FIELD: username,
            "email": email,
            "name": name
        }

        # Note that this could be accomplished in one try-except clause, but
        # instead we use get_or_create when creating unknown users since it has
        # built-in safeguards for multiple threads.
        if self.create_unknown_user:
            user, created = User.objects.get_or_create(
                **user_attributes
            )
            if created:
                user = self.configure_user(user)
            # If the user already exists we reconfigure them.
            else:
                user = self.reconfigure_user(user)
        else:
            try:
                user = User.objects.get_by_natural_key(username)
            except User.DoesNotExist:
                pass
        return user

    def configure_user(self, user):
        """
        We overwrite the default `configure_user` method from
        `RemoteUserBackend` to appropriatelly configure the user upon first
        login.

        The original code simply returns the `user` object. We are inspired by
        Sentry's `sentry/src/sentry/runner/commands/createuser.py` script for
        our custom code that assigns newly created users to the proper
        organization, roles and teams.
        """

        # Make sure the user is created as active
        user.is_active = True
        # Based on the `SENTRY_CERN_ADMINS` environment variable, translated
        # into the `CERN_ADMINS` setting, we have a list of usernames that
        # should be the administrators of this instance.
        if user.username in settings.CERN_ADMINS:
            # We set both the `is_superuser` and the `is_staff` flags same as
            # in the `createuser.py` script.
            # `is_superuser` designates that this user has all permissions
            # without explicitly assigning them.
            user.is_superuser = True
            # `is_staff` designates whether the user can log into this admin
            # site.
            user.is_staff = True
        # `is_managed` designates whether this user should be treated as
        # managed. If `True` the user is not allowed to modify their account.
        # Since the basic user information comes from SSO we want this enabled.
        user.is_managed = True
        # Make sure all the user modifications are saved.
        user.save()

        # For the newly created user (based on their ID) make sure their e-mail
        # (coming directly from SSO) is considered verified.
        UserEmail.objects.get(
            user_id=user.id
        ).update(
            email=user.email,
            is_verified=True
        )

        # As previously explained, we expect `SENTRY_SINGLE_ORGANIZATION` to
        # always be `True`.
        if settings.SENTRY_SINGLE_ORGANIZATION:

            # There is only 1 organization, the default one.
            org = Organization.get_default()
            # Based on the user's superuser status get the appropriate role ID.
            if user.is_superuser:
                role = roles.get_top_dog().id
            else:
                role = org.default_role
            # Add the user to the default organization.
            member = OrganizationMember.objects.create(
                organization=org,
                user=user,
                role=role
            )

            # If the user is an administrator add them to all the teams.
            if user.is_superuser:
                for team in Team.objects.all():
                    OrganizationMemberTeam.objects.create(
                        team=team,
                        organizationmember=member
                    )

        # Return the user object.
        return user

    def reconfigure_user(self, user):
        """
        Out custom `reconfigure_user` method reconfigures existing users when
        they login.
        """

        return user
