# Sentry at CERN

This repository builds a custom Sentry image that adds authentication for CERN via `HTTP` headers. It works with the [cern-sso-proxy](https://gitlab.cern.ch/paas-tools/cern-sso-proxy) after customizing the `httpd` configuration.

## `Dockerfile` file

Following the process suggested for building and running [Sentry On-Premise](https://github.com/getsentry/onpremise) we start with a `Dockerfile` that only contains the following:
```
FROM sentry:8.22-onbuild
```

Looking at the `sentry:8.22-onbuild` `Dockerfile` we see the following:
```
FROM sentry:8.22

WORKDIR /usr/src/sentry

# Add WORKDIR to PYTHONPATH so local python files don't need to be installed
ENV PYTHONPATH /usr/src/sentry
ONBUILD COPY . /usr/src/sentry

# Hook for installing additional plugins
ONBUILD RUN if [ -s requirements.txt ]; then pip install -r requirements.txt; fi

# Hook for installing a local app as an addon
ONBUILD RUN if [ -s setup.py ]; then pip install -e .; fi

# Hook for staging in custom configs
ONBUILD RUN if [ -s sentry.conf.py ]; then cp sentry.conf.py $SENTRY_CONF/; fi \
    && if [ -s config.yml ]; then cp config.yml $SENTRY_CONF/; fi
```

Essentially `sentry:8.22-onbuild` allows us to build a custom Sentry image simply by providing, among others, a custom `requirements.txt` file, custom `sentry.conf.py` and `config.yml` files and even custom local `Python` files (via the custom `PYTHONPATH`).

## `sentry.conf.py` file

We start by copying [`sentry.conf.py`](https://github.com/getsentry/docker-sentry/blob/master/8.22/sentry.conf.py) from the same Sentry release as defined in our `Dockerfile` (8.22). At the end of the file we add the following custom settings:

By default Sentry assumes that On-Premise installations will run as single organizations and that is our use case as well, where Sentry is meant to be run per project.

```
# Make sure `SENTRY_SINGLE_ORGANIZATION` is set to True. This instructs Sentry
# that this install intends to be run by a single organization and thus various
# UI optimizations should be enabled.
SENTRY_SINGLE_ORGANIZATION = True
```

Following django's guide on [Authentication using REMOTE_USER](https://django.readthedocs.io/en/1.6.x/howto/auth-remote-user.html) we need to use the `django.contrib.auth.middleware.RemoteUserMiddleware` in Sentry's `MIDDLEWARE_CLASSES` and `django.contrib.auth.backends.RemoteUserBackend` in Sentry's `AUTHENTICATION_BACKENDS`. We provide our custom middleware and backend, based on the original ones.
```
# In order to enable the REMOTE_USER authentication for Sentry 8.22 we follow
# the respective guide for django 1.6.11:
# <https://django.readthedocs.io/en/1.6.x/howto/auth-remote-user.html>.
# First we add our custom `CERNRemoteUserMiddleware` at the end of the existing
# `MIDDLEWARE_CLASSES` to make sure it comes after `AuthenticationMiddleware`.
MIDDLEWARE_CLASSES += ('CERN.CERNRemoteUserMiddleware',)
# Then we set `AUTHENTICATION_BACKENDS` to our custom `CERNRemoteUserBackend`.
AUTHENTICATION_BACKENDS = ('CERN.CERNRemoteUserBackend',)
```

In order to automatically assign the `superuser` role to specific users we create the custom `CERN_ADMINS` settings that reads a list of comma-separated usernames from the `SENTRY_CERN_ADMINS` environment variable.
```
# Finally we create a list of CERN usernames that will be the admins of this
# instance. The `SENTRY_CERN_ADMINS` environment variable contains comma
# separated usernames that will get the `superuser` status upon first login.
CERN_ADMINS = filter(
    None,
    [username.strip() for username in env("SENTRY_CERN_ADMINS", "").split(",")]
)
```

## `CERN` directory

The `CERN` directory is essentially a `Python` module that implements our custom authentication middleware and backend based on the respective original django ones.

`CERNRemoteUserMiddleware`:
* defines the names of our custom `HTTP` headers (`HTTP_X_ADFS_LOGIN`, `HTTP_X_ADFS_EMAIL` and `HTTP_X_ADFS_FULLNAME`),
* modifies the `process_request` method to:
    * assign our custom `HTTP` headers to custom local variables (`username`, `email` and `name`),
    * amend the call to `auth.authenticate` to pass our custom local variables as arguments.

`CERNRemoteUserBackend`:
* sets `create_unknown_user` to `True`, thus making sure users are created if they don't already exist
* modifies the `authenticate` method to:
    * accept our custom variables (`username`, `email` and `name`) as arguments,
    * call `User.objects.get_or_create` with our custom variables as arguments (so that new users are created with their `username`, `email` and `name`),
    * call the custom `reconfigure_user` method for users that log in but had already been created in the past,
* modifies the `configure_user` method to:
    * give the proper attributes to the user (`is_superuser`, `is_managed` etc),
    * make sure the user's email is considered verified (`is_verified`),
    * assign the user to the single default organization,
    * assign the user to all the existing teams in case they are a superuser,
* introduces the `reconfigure_user` method to:
    * do nothing for the time being, but remain as a way to reconfigure existing users in the future if needed.

For detailed line per line comments please refer to the `__init__.py` in the `CERN` directory.

## Other

Not part of this repository, but relevant to the `HTTP` headers mentioned earlier, we use a custom configuration for `httpd` at the [cern-sso-proxy](https://gitlab.cern.ch/paas-tools/cern-sso-proxy). `authorize.conf` looks something like:
```
# Make sure clients cannot fake authentication by injecting headers
RequestHeader unset X-ADFS-LOGIN
RequestHeader unset X-ADFS-EMAIL
RequestHeader unset X-ADFS-FULLNAME

# Unless the URL starts with "/api/\d+/store" apply the following:
<LocationMatch "^(?!/api/\d+/store)">
  ShibRequestSetting requireSession 1
  AuthType shibboleth
  <RequireALL>
    Require valid-user
    Require shib-attr ADFS_GROUP e-group-1 e-group-2
  </RequireALL>
  RequestHeader set X-ADFS-LOGIN %{ADFS_LOGIN}e
  RequestHeader set X-ADFS-EMAIL %{ADFS_EMAIL}e
  RequestHeader set X-ADFS-FULLNAME %{ADFS_FULLNAME}e
</LocationMatch>
```

As mentioned before we define our custom `HTTP` headers: `X-ADFS-LOGIN`, `X-ADFS-EMAIL` and `X-ADFS-FULLNAME` (note how they are respectively automatically translated to `HTTP_X_ADFS_LOGIN`, `HTTP_X_ADFS_EMAIL` and `HTTP_X_ADFS_FULLNAME` in django as mentioned [here](https://django.readthedocs.io/en/1.6.x/howto/auth-remote-user.html). We want to make sure all our traffic goes through Shibboleth authentication, except for the exceptions we send to Sentry via the DSN string. That means that all traffic directed to `/api/{PROJECT ID}/store/` should be skip the Shibboleth authentication and use the internal Sentry authentication instead. `<LocationMatch "^(?!/api/\d+/store)">` does exactly that: it matches URLS that do *not* start with `/api/\d+/store` (where `\d+` is a digit that appears one of more times, i.e. the Sentry `PROJECT ID`). For more information on the DSN string and its internals refer to the [latest documentation](https://github.com/getsentry/sentry-docs/blob/master/docs/clientdev/overview.rst).
